﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    interface Battle
    {
        void Start();

        void Review();
        
        bool IsFinished();
        
        void Continue();
        
        void ShowWinner();
    }
}
