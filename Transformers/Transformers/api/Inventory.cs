using System.Collections.Generic;

namespace Transformers
{
    public interface Inventory
    {
        List<Transformer> ListTransformers();

        List<Scanner> ListScannersFor(Transformer t);

        List<Weapon> ListWeaponsFor(Transformer t);
    }
}