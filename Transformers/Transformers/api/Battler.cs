﻿namespace Transformers
{
    public interface Battler
    {
        void Prepare(int id, FieldSetup battleField, Inventory inventory);

        void Act();

        string Name { get; }

        bool IsAlive { get; }
    }
}