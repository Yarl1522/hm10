﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    public interface Field
    {
        void MoveUnit(object unit, int shift);
        Transformer FindAny(Transformer owner, Scanner scanner);
    }
}
