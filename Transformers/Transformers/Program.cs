﻿using System;

namespace Transformers
{
    class Program
    {
        private readonly Battle battle;

        Program(Battle battle)
        {
            this.battle = battle;
        }

        void Run() 
        {
            for (battle.Start(); !battle.IsFinished(); battle.Review())
            {
                battle.Continue();
            }
            battle.ShowWinner();
        }

        static void Main(string[] args)
        {
            var battleField = new TransformerBattleField();
            var inventory = new TransformersInventory();
            inventory.Setup(battleField);

            var battle = new StepwiseBattle(
                battleField,
                inventory,
                new TransformerBattler(), 
                new TransformerBattler());
            new Program(battle).Run();
        }
    }

}
