﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    class OpticalScanner : BaseScanner
    {
        private readonly string model;

        public OpticalScanner(string model, int distance) : base(distance)
        {
            this.model = model;
        }

        public override string ToString()
        {
            return $"Optical scanner {model}";
        }
    }
}
