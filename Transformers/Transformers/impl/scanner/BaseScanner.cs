﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    class BaseScanner: Scanner
    {
        private readonly int distance;
        private Transformer owner;

        protected BaseScanner(int distance)
        {
            this.distance = distance;
        }

        public void AttachTo(Transformer t)
        {
            owner = t;
        }

        public bool CanScanTargetFrom(int targetPos, int sourcePos)
        {
            return Math.Abs(targetPos - sourcePos) <= distance;
        }

        public Transformer Scan(Field field)
        {
            return field.FindAny(owner, this);
        }
    }
}
