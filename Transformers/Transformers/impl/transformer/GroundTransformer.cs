﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    class GroundTransformer : BaseTransformer
    {
        public GroundTransformer(string name, Field field) : base(name, field)
        {
        }

        public override void Move(int speed)
        {
            if (IsHumanoid)
            {
                Console.WriteLine($"{Name} runs at speed {speed}");
            }
            else {
                Console.WriteLine($"{Name} drives at speed {speed}");
            }
            base.Move(speed);
        }
    }
}
