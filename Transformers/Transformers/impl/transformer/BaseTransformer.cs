﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    class BaseTransformer: Transformer
    {
        private string name;
        private int health;
        private bool isHumanoid = false;
        private readonly Field field;
        private Scanner scanner;
        private Weapon weapon;
        
        private Transformer foundEnemy;

        public BaseTransformer(string name, Field field)
        {
            this.name = name;
            this.field = field;
            this.health = 100;
        }

        public string Name => name;

        public bool IsHumanoid => isHumanoid;

        public int Health => health;

        public bool FindEnemy()
        {
            Console.WriteLine($"{Name} scans with {scanner}");
            foundEnemy = scanner.Scan(field);
            return foundEnemy != null;
        }

        public void Fire()
        {
            if (isHumanoid && weapon != null)
            {
                Console.WriteLine($"{Name} fires with {weapon}");
                weapon.Shoot(foundEnemy);
            }
        }

        public virtual void Move(int speed)
        {
            field.MoveUnit(this, speed);
        }

        public void Transform()
        {
            isHumanoid = !isHumanoid;
            Console.WriteLine($"{Name} transformed");
        }

        public void AddScanner(Scanner scanner)
        {
            this.scanner = scanner;
            this.scanner.AttachTo(this);
        }

        public void AddWeapon(Weapon weapon)
        {
            this.weapon = weapon;
        }

        public void Shot(int damage)
        {
            Console.WriteLine($"{Name} has received damage {damage}");
            this.health -= damage;
        }
    }
}
