﻿using System;

namespace Transformers
{
    internal class TransformerBattler: Battler
    {
        private Transformer transformer;

        public string Name => transformer.Name;

        public bool IsAlive => transformer.Health > 0;

        public void Act()
        {
            if (transformer.Health < 5)
            {
                return;
            }

            bool found = transformer.FindEnemy();

            if (found)
            {
                if (transformer.Health < 20)
                {
                    transformer.Move(-1);
                    return;
                }
                if (!transformer.IsHumanoid)
                {
                    transformer.Transform();
                }
                transformer.Fire();
                transformer.Move(1);
            }
            else 
            {
                if (transformer.IsHumanoid)
                {
                    transformer.Transform();
                }
                transformer.Move(2);
            }
        }

        public void Prepare(int id, FieldSetup battleField, Inventory inventory)
        {
            Console.WriteLine($"Please choose transformer for battler {id}:");
            int tid = int.Parse(Console.ReadLine());
            this.transformer = inventory.ListTransformers()[tid - 1];

            Console.WriteLine($"Please choose scanner for {Name} from list below:");
            var scanners = inventory.ListScannersFor(this.transformer);
            for (int i = 0; i < scanners.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {scanners[i]}");
            }
            int sid = int.Parse(Console.ReadLine());
            this.transformer.AddScanner(scanners[sid - 1]);

            Console.WriteLine($"Please choose weapon for {Name} from list below:");
            var weapons = inventory.ListWeaponsFor(this.transformer);
            for (int j = 0; j < weapons.Count; j++)
            {
                Console.WriteLine($"{j + 1}. {weapons[j]}");
            }
            int wid = int.Parse(Console.ReadLine());
            this.transformer.AddWeapon(weapons[wid - 1]);

            Console.WriteLine($"Enter position for {Name}:");
            int pos = int.Parse(Console.ReadLine());
            battleField.AddUnit(transformer, pos);
        }
    }
}